package pl.edu.agh.influencer.utlis

class Utils{
    companion object {
        private const val blueOutput = "\u001B[34m"
        private const val resetColor = "\u001B[0m"

        fun printBlueOutput(text: String){
            println("$blueOutput$text$resetColor")
        }
    }
}