package pl.edu.agh.influencer.models.model2

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import pl.edu.agh.influencer.entities.*
import pl.edu.agh.influencer.utlis.Utils
import java.io.File
import java.io.FileWriter
import kotlin.math.sqrt

class Model2 {
    fun countAndInsertTags() {
        transaction {
            if (!Model2Tags.exists()) {
                Utils.printBlueOutput("Counting tags:")
                addLogger(StdOutSqlLogger)
                SchemaUtils.createMissingTablesAndColumns(Model2Tags)
                (((Tags innerJoin PostTags)
                        .slice(Tags.id, Tags.name, PostTags.tagsId.count())
                        .selectAll())
                        .groupBy(Tags.id))
                        .forEach { result ->
                            if (result[PostTags.tagsId.count()] > 1) {
                                Model2Tags.insert {
                                    it[Model2Tags.id] = result[Tags.id]
                                    it[Model2Tags.name] = result[Tags.name]
                                    it[Model2Tags.count] = result[PostTags.tagsId.count()]
                                }
                            }
                        }
                Utils.printBlueOutput("Ends counting tags...")
            }
        }
    }

    fun countPostsImportanceDependsOnTagsValues() {
        transaction {
            if (!Model2Posts.exists()) {
                Utils.printBlueOutput("Counting posts importance depends on tags values:")
                addLogger(StdOutSqlLogger)
                SchemaUtils.createMissingTablesAndColumns(Model2Posts)
                ((((Posts innerJoin Comments)
                        .slice(Posts.id, Posts.authorId, Posts.date, Comments.id.count()))
                        .selectAll())
                        .groupBy(Posts.id))
                        .forEach { result ->
                            val postId = result[Posts.id]
                            val authorId = result[Posts.authorId]
                            val commentsCount = result[Comments.id.count()].toFloat()
//                            val tagsCount: Long? = (((Model2Tags innerJoin Tags) innerJoin PostTags) innerJoin Posts)
//                                    .slice(Model2Tags.count.sum())
//                                    .select { Op.build { PostTags.postsId eq postId } }
//                                    .toList().first().data.first() as Long?
//                            Model2Posts.insert {
//                                it[Model2Posts.id] = postId
//                                it[Model2Posts.authorId] = authorId
//                                it[Model2Posts.importance] = if (tagsCount != null && tagsCount != 0L) commentsCount / tagsCount.toFloat() else sqrt(commentsCount)
//                                it[Model2Posts.commentsCount] = commentsCount.toInt()
//                                it[Model2Posts.tagsValue] = tagsCount?.toInt() ?: 0
//                                it[Model2Posts.date] = result[Posts.date]
//                            }
                        }
                Utils.printBlueOutput("Ends counting importance...")
            }
        }
    }

    fun countSummaryForAuthors() {
        transaction {
            if (!Model2AuthorsSummary.exists()) {
                Utils.printBlueOutput("Counting summary for authors:")
                addLogger(StdOutSqlLogger)
                SchemaUtils.createMissingTablesAndColumns(Model2AuthorsSummary)
                (Authors innerJoin Model2Posts).slice(Authors.id, Authors.name, Model2Posts.id.count(), Model2Posts.importance.avg())
                        .selectAll()
                        .groupBy(Authors.id)
                        .having { Op.build { Model2Posts.id.count() greaterEq 25 } }
                        .forEach { result ->
                            val commentsCount = Comments.select { Op.build { Comments.authorId eq result[Authors.id] } }.count()
                            Model2AuthorsSummary.insert {
                                it[Model2AuthorsSummary.id] = result[Authors.id]
                                it[Model2AuthorsSummary.name] = result[Authors.name] ?: ""
                                it[Model2AuthorsSummary.postsCount] = result[Model2Posts.id.count()]
                                it[Model2AuthorsSummary.avgPostsImportance] = result[Model2Posts.importance.avg()]?.toFloat() ?: 0f
                                it[Model2AuthorsSummary.commentsCount] = commentsCount
                            }
                        }
                Utils.printBlueOutput("Ends counting summary...")
            }
        }
    }

    fun countLongPeriodForAuthors() {
        transaction {
            if (!Model2AuthorsLong.exists()) {
                Utils.printBlueOutput("Counting long period for authors:")
                addLogger(StdOutSqlLogger)
                SchemaUtils.createMissingTablesAndColumns(Model2AuthorsLong)
                val dates = Model2Posts.slice(Model2Posts.date).selectAll().orderBy(Model2Posts.date)
                var startPeriod = dates.first()[Model2Posts.date]
                var midPeriod = startPeriod.plusMonths(3)
                val endPeriod = dates.last()[Model2Posts.date]
                while (!startPeriod.isAfter(endPeriod)) {
                    (Model2Posts innerJoin Authors).slice(Model2Posts.authorId, Model2Posts.id.count(), Model2Posts.importance.avg())
                            .selectAll()
                            .groupBy(Model2Posts.authorId)
                            .andWhere { Op.build { Model2Posts.date greaterEq startPeriod }.and(Op.build { Model2Posts.date less midPeriod }) }
                            .forEach { result ->
                                val commentsCount = Comments.slice(Comments.id).select {
                                    Op.build { Comments.authorId eq result[Model2Posts.authorId] }
                                            .and((Op.build { Comments.date greaterEq startPeriod }
                                                    .and(Op.build { Comments.date less midPeriod })))
                                }
                                        .count()
                                val authorName = Authors.slice(Authors.name).select { Op.build { Authors.id eq result[Model2Posts.authorId]!! } }.first()[Authors.name]
                                        ?: ""
                                Model2AuthorsLong.insert {
                                    it[Model2AuthorsLong.authorId] = result[Model2Posts.authorId] ?: 0
                                    it[Model2AuthorsLong.name] = authorName
                                    it[Model2AuthorsLong.postsCount] = result[Model2Posts.id.count()]
                                    it[Model2AuthorsLong.avgPostsImportance] = result[Model2Posts.importance.avg()]?.toFloat() ?: 0f
                                    it[Model2AuthorsLong.commentsCount] = commentsCount
                                    it[Model2AuthorsLong.startPeriod] = startPeriod
                                    it[Model2AuthorsLong.endPeriod] = midPeriod
                                }
                            }
                    DateTime()
                    startPeriod = midPeriod
                    midPeriod = midPeriod.plusMonths(3)
                }
                Utils.printBlueOutput("Ends counting long period...")
            }
        }
    }

    fun saveTopXResultsToFile(limit: Int) {
        val file = File("model2Top${limit}results.csv")
        if (!file.exists()) {
            transaction {
                val writer = FileWriter(file)
                try {
                    file.createNewFile()
                    writer.append("ID, name, AVG importance, comments written, posts count\n")
                    Model2AuthorsSummary.selectAll()
                            .orderBy(Model2AuthorsSummary.avgPostsImportance, false)
                            .limit(limit)
                            .forEach { result ->
                                writer.append("${result[Model2AuthorsSummary.id]}, ")
                                writer.append("${result[Model2AuthorsSummary.name]}, ")
                                writer.append("${result[Model2AuthorsSummary.avgPostsImportance]}, ")
                                writer.append("${result[Model2AuthorsSummary.commentsCount]}, ")
                                writer.append("${result[Model2AuthorsSummary.postsCount]}\n")
                            }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    writer.flush()
                    writer.close()
                }
            }
        }
    }

}