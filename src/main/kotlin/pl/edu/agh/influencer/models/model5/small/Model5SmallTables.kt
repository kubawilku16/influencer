package pl.edu.agh.influencer.models.model5.small

import org.jetbrains.exposed.sql.Table
import pl.edu.agh.influencer.entities.Authors
import pl.edu.agh.influencer.entities.TimeStamps

object Model5SmallResults : Table("model5_small_results") {
    val author = integer("author_id") references Authors.id
    val timestamp = integer("timestamp").references(TimeStamps.id)
    val result = double("result")
}