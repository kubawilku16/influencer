package pl.edu.agh.influencer.models.model5

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import pl.edu.agh.influencer.entities.Comments
import pl.edu.agh.influencer.entities.Posts
import pl.edu.agh.influencer.entities.TimeStamp
import pl.edu.agh.influencer.entities.TimeStamps
import pl.edu.agh.influencer.utlis.Utils
import kotlin.math.pow

class Model5 {
    fun execute() {
        var timestamps = listOf<TimeStamp>()
        transaction {
            if (!Model5Results.exists()) {
                addLogger(StdOutSqlLogger)
                SchemaUtils.createMissingTablesAndColumns(Model5Results)
            }
            timestamps = TimeStamps
                    .select(Op.build {
                        TimeStamps.id notInList
                                Model5Results.selectAll().map { EntityID(it[Model5Results.timestamp], TimeStamps) }
                    })
                    .map { TimeStamp.wrapRow(it) }
        }
        timestamps.forEach { period ->
            transaction {
                addLogger(StdOutSqlLogger)
                Utils.printBlueOutput("Period: ${period.start.toString("dd-MM-YYYY")}-${period.end.toString("dd-MM-YYYY")}")
                val postsPerPeriod = Posts.selectAll()
                        .adjustWhere {
                            Op.build { Posts.date greaterEq period.start }
                                    .and(Op.build { Posts.date less period.end })
                        }
                postsPerPeriod.map { it[Posts.authorId] }.toSet().forEach { author ->
                    val authorPosts = postsPerPeriod.adjustWhere { Op.build { Posts.authorId eq author } }.map { it[Posts.id] }
                    val receivedComments =
                            Comments.select(
                                    Op.build { Comments.postId inList authorPosts }.and(Op.build { Comments.authorId neq author })
                            ).map { it[Comments.authorId] }
                    val currentResult = receivedComments.toSet().size.toDouble()
                            .div(receivedComments.size) * authorPosts.size
                    val lastResult = getLastResult(author, period.id.value)
                    Model5Results.insert {
                        it[Model5Results.author] = author!!
                        it[Model5Results.timestamp] = period.id.value
                        it[Model5Results.result] = currentResult + lastResult
                    }
                }

            }
        }
    }

    private fun getLastResult(author: Int?, period: Int): Double {
        return author?.let {
            var result = 0.0
            var lastResult: ResultInfo? = null
            transaction {
                try {
                    lastResult = Model5Results
                            .select(Op.build { Model5Results.timestamp less period }.and(Op.build { Model5Results.author eq author }))
                            .limit(1)
                            .map { ResultInfo(it[Model5Results.timestamp], it[Model5Results.result]) }[0]
                } catch (ignored: Exception) {
                }
            }
            lastResult?.let {
                result = it.value * 0.5.pow(period - it.periodId)
            }
            result
        } ?: 0.0
    }

    private class ResultInfo(val periodId: Int, val value: Double)
}