package pl.edu.agh.influencer.models.model4

import org.jetbrains.exposed.sql.Table
import pl.edu.agh.influencer.entities.Authors

object Model4Results : Table("model4_results") {
    val id = integer("id") references Authors.id
    val name = text("name")
    val result = double("result")
}