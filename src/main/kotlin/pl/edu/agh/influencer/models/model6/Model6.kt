package pl.edu.agh.influencer.models.model6

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import pl.edu.agh.influencer.entities.*
import pl.edu.agh.influencer.models.model3.Model3Results
import pl.edu.agh.influencer.models.model4.Model4Results
import pl.edu.agh.influencer.utlis.Utils
import java.io.File
import java.io.FileWriter

class Model6 {

    private var minimalAuthorsWage = 0.0
    private var timestamps = listOf<TimeStamp>()
    private val authors = mutableListOf<Int>()
    private val posts = mutableListOf<PostInfo>()
    private val wages = mutableMapOf<Int, Double>()
    private val results = mutableMapOf<Int, Double>()


    fun execute() {
        initData()
        timestamps.forEach { period ->
            transaction {
                addLogger(StdOutSqlLogger)
                Utils.printBlueOutput("Period(${timestamps.indexOf(period)}): ${period.start.toString("dd-MM-YYYY")}-${period.end.toString("dd-MM-YYYY")}")
                val postsPerPeriod = posts.filter { (it.date.isAfter(period.start) || it.date.isEqual(period.start)) && it.date.isBefore(period.end) }
                postsPerPeriod.map { it.author }.toSet().forEach { author ->
                    val authorPosts = postsPerPeriod.filter { it.author == author }.map { it.id }
                    val result = Comments
                            .select(Op.build { Comments.postId inList authorPosts })
                            .map { wages[it[Comments.authorId]] ?: minimalAuthorsWage }
                            .sum()
                    results[author] = results[author]!! + result
                }
                results.forEach { author, value ->
                    Model6Result.insert {
                        it[Model6Result.author] = author
                        it[Model6Result.timestamp] = period.id.value
                        it[Model6Result.result] = value
                    }

                }
            }
        }
    }

    fun extractResults() {
        val file = File("period_diff_comm_model_result.csv")
        if (!file.exists()) {
            transaction {
                addLogger(StdOutSqlLogger)
                val writer = FileWriter(file)
                try {
                    file.createNewFile()
                    writer.append("Author")
                    TimeStamps.selectAll().orderBy(TimeStamps.id).map { TimeStamp.wrapRow(it) }
                            .forEach { period -> writer.append(", ${period.start.toString("dd-MM-YYYY")}-${period.end.toString("dd-MM-YYYY")}") }
                    writer.append("\n")
                    val results = Model6Result.leftJoin(Authors)
                            .slice(Model6Result.author, Authors.name, Model6Result.timestamp, Model6Result.result)
                            .selectAll()
                            .map { AuthorResult(it[Model6Result.author], it[Authors.name]!!, it[Model6Result.timestamp], it[Model6Result.result]) }
                            .toList()
                    Model6Result.slice(Model6Result.author)
                            .selectAll()
                            .orderBy(Model6Result.result, false)
                            .map { it[Model6Result.author] }
                            .toSet()
                            .forEach { author ->
                                writer.append(results.first { author == it.id }.name)
                                repeat(288) { period ->
                                    writer.append(", ${results
                                            .firstOrNull { it.id == author && it.period == period }?.value ?: 0.0}")
                                }
                                writer.append("\n")
                            }

                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    writer.flush()
                    writer.close()
                }
            }
        }
    }

    private class AuthorResult(val id: Int, val name: String, val period: Int, val value: Double)

    private fun initData() {
        transaction {
            addLogger(StdOutSqlLogger)
            if (!Model6Result.exists()) {
                SchemaUtils.createMissingTablesAndColumns(Model6Result)
            }
            minimalAuthorsWage = 1.0 / Authors.selectAll().count()
            timestamps = TimeStamps
                    .select(Op.build {
                        TimeStamps.id notInList
                                Model6Result.selectAll().map { EntityID(it[Model6Result.timestamp], TimeStamps) }
                    })
                    .map { TimeStamp.wrapRow(it) }
            authors.addAll(Model4Results
                    .slice(Model4Results.id)
                    .selectAll()
                    .orderBy(Model4Results.result, false)
                    .limit(25)
                    .map { it[Model4Results.id] }.toList())
            authors.forEach { results[it] = 0.0 }
            posts.addAll(Posts
                    .select { Op.build { Posts.authorId inList authors } }
                    .map { PostInfo(it[Posts.id], it[Posts.authorId] ?: 0, it[Posts.date]) }
            )
            Model3Results
                    .slice(Model3Results.authorId, Model3Results.rank1)
                    .selectAll()
                    .orderBy(Model3Results.rank1)
                    .forEach {
                        wages[it[Model3Results.authorId]] = 1.0 / (it[Model3Results.rank1] + 1.0)
                    }
        }
    }

    private class PostInfo(val id: Int, val author: Int, val date: DateTime)
}