package pl.edu.agh.influencer.models.model5.small

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import pl.edu.agh.influencer.entities.*
import pl.edu.agh.influencer.models.model4.Model4Results
import pl.edu.agh.influencer.utlis.Utils
import java.io.File
import java.io.FileWriter
import kotlin.math.pow

class Model5Small {
    fun execute() {
        var timestamps = listOf<TimeStamp>()
        val authors = mutableListOf<Int>()
        val posts = mutableListOf<PostInfo>()
        transaction {
            if (!Model5SmallResults.exists()) {
                addLogger(StdOutSqlLogger)
                SchemaUtils.createMissingTablesAndColumns(Model5SmallResults)
            }
            timestamps = TimeStamps
                    .select(Op.build {
                        TimeStamps.id notInList
                                Model5SmallResults.selectAll().map { EntityID(it[Model5SmallResults.timestamp], TimeStamps) }
                    })
                    .map { TimeStamp.wrapRow(it) }
            authors.addAll(Model4Results.slice(Model4Results.id)
                    .selectAll()
                    .orderBy(Model4Results.result, false)
                    .limit(100)
                    .map { it[Model4Results.id] }.toList())
            posts.addAll(Posts.select { Op.build { Posts.authorId inList authors } }.map {
                PostInfo(it[Posts.id], it[Posts.authorId] ?: 0, it[Posts.date])
            })

        }
        timestamps.forEach { period ->
            transaction {
                addLogger(StdOutSqlLogger)
                Utils.printBlueOutput("Period: ${period.start.toString("dd-MM-YYYY")}-${period.end.toString("dd-MM-YYYY")}")
                val postsPerPeriod = posts.filter { (it.date.isAfter(period.start) || it.date.isEqual(period.start)) && it.date.isBefore(period.end) }
                postsPerPeriod.map { it.author }.forEach { author ->
                    val authorPosts = postsPerPeriod.filter { it.author == author }.map { it.id }
                    val receivedComments =
                            Comments.select(
                                    Op.build { Comments.postId inList authorPosts }.and(Op.build { Comments.authorId neq author })
                            ).map { it[Comments.authorId] }
                    val currentResult = receivedComments.toSet().size.toDouble()
                            .div(receivedComments.size) * authorPosts.size
                    val lastResult = getLastResult(author, period.id.value)
                    Model5SmallResults.insert {
                        it[Model5SmallResults.author] = author
                        it[Model5SmallResults.timestamp] = period.id.value
                        it[Model5SmallResults.result] = currentResult + lastResult
                    }
                }
            }
        }
    }

    fun extractResults() {
        val file = File("period_model_result.csv")
        if (!file.exists()) {
            transaction {
                addLogger(StdOutSqlLogger)
                val writer = FileWriter(file)
                try {
                    file.createNewFile()
                    writer.append("Author")
                    TimeStamps.selectAll().orderBy(TimeStamps.id).map { TimeStamp.wrapRow(it) }
                            .forEach { period -> writer.append(", ${period.start.toString("dd-MM-YYYY")}-${period.end.toString("dd-MM-YYYY")}") }
                    writer.append("\n")
                    val results = Model5SmallResults.leftJoin(Authors)
                            .slice(Model5SmallResults.author, Authors.name, Model5SmallResults.timestamp, Model5SmallResults.result)
                            .selectAll()
                            .map { AuthorResult(it[Model5SmallResults.author], it[Authors.name]!!, it[Model5SmallResults.timestamp], it[Model5SmallResults.result]) }
                            .toList()
                    Model4Results.slice(Model4Results.id)
                            .selectAll()
                            .orderBy(Model4Results.result, false)
                            .limit(100)
                            .map { it[Model4Results.id] }.toList().forEach { author ->
                                writer.append(results.first { author == it.id }.name)
                                repeat(288) { period ->
                                    writer.append(", ${results
                                            .firstOrNull { it.id == author && it.period == period }?.value ?: 0.0}")
                                }
                                writer.append("\n")
                            }

                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    writer.flush()
                    writer.close()
                }
            }
        }
    }

    private fun getLastResult(author: Int?, period: Int): Double {
        return author?.let {
            var result = 0.0
            var lastResult: ResultInfo? = null
            transaction {
                try {
                    lastResult = Model5SmallResults
                            .select(Op.build { Model5SmallResults.timestamp less period }.and(Op.build { Model5SmallResults.author eq author }))
                            .limit(1)
                            .map { ResultInfo(it[Model5SmallResults.timestamp], it[Model5SmallResults.result]) }[0]
                } catch (ignored: Exception) {
                }
            }
            lastResult?.let {
                result = it.value * 0.5.pow(period - it.periodId)
            }
            result
        } ?: 0.0
    }

    private class AuthorResult(val id: Int, val name: String, val period: Int, val value: Double)
    private class PostInfo(val id: Int, val author: Int, val date: DateTime)
    private class ResultInfo(val periodId: Int, val value: Double)
}