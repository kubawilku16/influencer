package pl.edu.agh.influencer.models.model3

import org.jetbrains.exposed.sql.Table
import pl.edu.agh.influencer.entities.Authors

object Model3AuthorReceivedComments : Table("model_3_author_received_comments") {
    val authorId = integer("post_author_id") references Authors.id
    val commentatorId = integer("commentator_id") references Authors.id
    val commentsCount = integer("comments_count")
}

object Model3Results : Table("model3_results") {
    val authorId = integer("author_id") references Authors.id
    val name = text("author_name")
    val commentsCount = integer("comments_count")
    val differentCommentators = integer("diff_commentators")
    val postsCount = integer("postsCount")
    val rank1 = integer("rank_by_diff_commentators")
    val rank2 = integer("rank_by_comm_count_per_post")
}