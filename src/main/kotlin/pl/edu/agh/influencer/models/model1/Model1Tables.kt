package pl.edu.agh.influencer.models.model1

import org.jetbrains.exposed.sql.Table
import pl.edu.agh.influencer.entities.Authors
import pl.edu.agh.influencer.entities.Posts
import pl.edu.agh.influencer.entities.Tags

object Model1Tags : Table(name = "model_1_tags") {
    val id = integer("id").primaryKey() references Tags.id
    val name = varchar("name", length = 255).nullable()
    val count = integer("count")
}

object Model1Posts : Table("model_1_posts") {
    val id = integer("id") references Posts.id
    val authorId = (integer("author_id") references Authors.id).nullable()
    val importance = float("importance") // Number of comments / Tags value or if Tags value equals 0, then just sqrt of comments count  ?? or to discuss
}

object Model1Authors : Table("model_1_authors") {
    val id = integer("id").primaryKey() references Authors.id
    val name = varchar("name", length = 255)
    val avgPostsImportance = float("avg_importance")
    val maxImportance = float("max_importance")
    val minImportance = float("min_importance")
    val postsCount = integer("posts_count")
    val commentsCount = integer("comments_count")
}