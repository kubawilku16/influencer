package pl.edu.agh.influencer.models.model2

import org.jetbrains.exposed.sql.Table
import pl.edu.agh.influencer.entities.Authors
import pl.edu.agh.influencer.entities.Posts
import pl.edu.agh.influencer.entities.Tags

object Model2Tags : Table(name = "model_2_tags") {
    val id = integer("id").primaryKey() references Tags.id
    val name = varchar("name", length = 255).nullable()
    val count = integer("count")
}

object Model2Posts : Table("model_2_posts") {
    val id = integer("id") references Posts.id
    val authorId = (integer("author_id") references Authors.id).nullable()
    val importance = float("importance")
    val commentsCount = integer("comments")
    val tagsValue = integer("tags")
    val date = date("date")
}

object Model2AuthorsSummary : Table("model_2_authors_summary") {
    val id = integer("id").primaryKey() references Authors.id
    val name = varchar("name", length = 255)
    val avgPostsImportance = float("avg_importance")
    val postsCount = integer("posts_count")
    val commentsCount = integer("comments_count")
}

object Model2AuthorsLong : Table("model_2_authors_long") {
    val id = integer("id").autoIncrement().primaryKey()
    val authorId = integer("authorId") references Authors.id
    val name = varchar("name", length = 255)
    val avgPostsImportance = float("avg_importance")
    val postsCount = integer("posts_count")
    val commentsCount = integer("comments_count")
    val startPeriod = datetime("start_period")
    val endPeriod = datetime("end_period")
}

object Model2AuthorsShort : Table("model_2_authors_long") {
    val id = integer("id").autoIncrement().primaryKey()
    val authorId = integer("authorId") references Authors.id
    val name = varchar("name", length = 255)
    val avgPostsImportance = float("avg_importance")
    val postsCount = integer("posts_count")
    val commentsCount = integer("comments_count")
    val startPeriod = datetime("start_period")
    val endPeriod = datetime("end_period")
}
