package pl.edu.agh.influencer.models.model1

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import pl.edu.agh.influencer.entities.*
import pl.edu.agh.influencer.utlis.Utils.Companion.printBlueOutput
import java.io.File
import java.io.FileWriter
import kotlin.math.sqrt

class Model1 {

    fun countAndInsertTags() {
        transaction {
            if (!Model1Tags.exists()) {
                printBlueOutput("Counting tags:")
                addLogger(StdOutSqlLogger)
                SchemaUtils.createMissingTablesAndColumns(Model1Tags)
                (((Tags innerJoin PostTags)
                        .slice(Tags.id, Tags.name, PostTags.tagsId.count())
                        .selectAll())
                        .groupBy(Tags.id))
                        .forEach { result ->
                            Model1Tags.insert {
                                it[Model1Tags.id] = result[Tags.id]
                                it[Model1Tags.name] = result[Tags.name]
                                it[Model1Tags.count] = result[PostTags.tagsId.count()]
                            }
                        }
                printBlueOutput("Ends counting tags...")
            }
        }
    }

    fun countPostsImportanceDependsOnTagsValues() {
        transaction {
            if (!Model1Posts.exists()) {
                printBlueOutput("Counting posts importance depends on tags values:")
                addLogger(StdOutSqlLogger)
                SchemaUtils.createMissingTablesAndColumns(Model1Posts)
                ((((Posts innerJoin Comments)
                        .slice(Posts.id, Posts.authorId, Comments.id.count()))
                        .selectAll())
                        .groupBy(Posts.id))
                        .forEach { result ->
                            val postId = result[Posts.id]
                            val authorId = result[Posts.authorId]
                            val commentsCount = result[Comments.id.count()].toFloat()
//                            val tagsCount: Long? = (((Model1Tags innerJoin Tags) innerJoin PostTags) innerJoin Posts)
//                                    .slice(Model1Tags.count.sum())
//                                    .select { Op.build { PostTags.postsId eq postId } }
//                                    .toList().first().data.first() as Long?
//                            Model1Posts.insert {
//                                it[Model1Posts.id] = postId
//                                it[Model1Posts.authorId] = authorId
//                                it[Model1Posts.importance] = if (tagsCount != null && tagsCount != 0L) sqrt(commentsCount) / tagsCount.toFloat() else commentsCount
//                            }
                        }
                printBlueOutput("Ends counting importance...")
            }
        }
    }

    fun countAuthorsData() {
        transaction {
            if (!Model1Authors.exists()) {
                printBlueOutput("Resolving authors final data:")
                addLogger(StdOutSqlLogger)
                SchemaUtils.createMissingTablesAndColumns(Model1Authors)
                (Authors innerJoin Model1Posts)
                        .slice(Authors.id, Authors.name, Model1Posts.id.count(),
                                Model1Posts.importance.min(), Model1Posts.importance.max(), Model1Posts.importance.avg())
                        .selectAll()
                        .groupBy(Authors.id)
                        .forEach { author ->
                            val id = author[Authors.id]
                            val name = author[Authors.name]
                            val postsCount = author[Model1Posts.id.count()]
//                            val min = (author.data[3] as Double).toFloat()
//                            val max = (author.data[4] as Double).toFloat()
//                            val avg = (author.data[5] as Double).toFloat()
//                            val commentsCount = Comments.select { Op.build { Comments.authorId eq id } }.count()
//                            Model1Authors.insert {
//                                it[Model1Authors.id] = id
//                                it[Model1Authors.name] = name ?: ""
//                                it[Model1Authors.postsCount] = postsCount
//                                it[Model1Authors.commentsCount] = commentsCount
//                                it[Model1Authors.minImportance] = min
//                                it[Model1Authors.maxImportance] = max
//                                it[Model1Authors.avgPostsImportance] = avg
//                            }
                        }
                printBlueOutput("Ends resolving authors data...")
            }
        }
    }

    fun saveTopXResultsToFile(limit: Int) {
        val file = File("model1Top${limit}results.csv")
        if (!file.exists()) {
            transaction {
                val writer = FileWriter(file)
                try {
                    file.createNewFile()
                    writer.append("ID, name, AVG importance, comments written, posts count\n")
                    Model1Authors.selectAll()
                            .orderBy(Model1Authors.avgPostsImportance, false)
                            .limit(limit)
                            .forEach { result ->
                                writer.append("${result[Model1Authors.id]}, ")
                                writer.append("${result[Model1Authors.name]}, ")
                                writer.append("${result[Model1Authors.avgPostsImportance]}, ")
                                writer.append("${result[Model1Authors.commentsCount]}, ")
                                writer.append("${result[Model1Authors.postsCount]}\n")
                            }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    writer.flush()
                    writer.close()
                }
            }
        }
    }
}