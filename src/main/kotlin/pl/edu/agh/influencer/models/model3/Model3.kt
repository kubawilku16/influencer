package pl.edu.agh.influencer.models.model3

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import pl.edu.agh.influencer.entities.Authors
import pl.edu.agh.influencer.entities.Comments
import pl.edu.agh.influencer.entities.Posts
import pl.edu.agh.influencer.utlis.Utils
import java.util.*
import kotlin.collections.HashSet

class Model3 {

    fun countModel3() {
        val authors = HashSet<Int>()
        transaction {
            Utils.printBlueOutput("Getting all authors")
            addLogger(StdOutSqlLogger)
            SchemaUtils.createMissingTablesAndColumns(Model3AuthorReceivedComments)
            authors.addAll((
                    Posts.slice(Posts.authorId)
                            .select(Op.build {
                                Posts.authorId notInList
                                        (Model3AuthorReceivedComments
                                                .slice(Model3AuthorReceivedComments.authorId)
                                                .selectAll()
                                                .map { it[Model3AuthorReceivedComments.authorId] }
                                                .toSet()
                                                .toList())
                            }).map { it[Posts.authorId]!! }
                            .toList()))
        }
        authors.forEach { id ->
            transaction {
                val resultMap: MutableMap<Int, Int> = HashMap()
                Utils.printBlueOutput("Getting all posts for authorID = $id")
                addLogger(StdOutSqlLogger)
                val posts: Set<Int> =
                        Posts.slice(Posts.id)
                                .select(Op.build { Posts.authorId eq id })
                                .map { result -> result[Posts.id] }
                                .toHashSet()
                posts.forEach { postId ->
                    Comments.slice(Comments.authorId).select(Op.build { Comments.postId eq postId })
                            .forEach { it ->
                                val commentId = it[Comments.authorId] ?: 0
                                if (commentId != id) {
                                    if (!resultMap.containsKey(commentId)) {
                                        resultMap[commentId] = 0
                                    }
                                    resultMap[commentId] = resultMap[commentId]!!.plus(1)
                                }
                            }
                }
                Utils.printBlueOutput("Inserting values for authorID = $id")
                resultMap.forEach { (commentator, value) ->
                    Model3AuthorReceivedComments.insert {
                        it[Model3AuthorReceivedComments.authorId] = id
                        it[Model3AuthorReceivedComments.commentatorId] = commentator
                        it[Model3AuthorReceivedComments.commentsCount] = value
                    }
                }
                Utils.printBlueOutput("End inserting values for authorID = $id")
            }
        }
    }

    fun countModel3Parallel() {
        val authors = HashSet<Int>()
        val posts = LinkedList<M3Post>()
        val comments = LinkedList<M3Comment>()
        transaction {
            Utils.printBlueOutput("Getting all authors")
            addLogger(StdOutSqlLogger)
            SchemaUtils.createMissingTablesAndColumns(Model3AuthorReceivedComments)
            authors.addAll((
                    Posts.slice(Posts.authorId)
                            .select(Op.build {
                                Posts.authorId notInList
                                        (Model3AuthorReceivedComments
                                                .slice(Model3AuthorReceivedComments.authorId)
                                                .selectAll()
                                                .map { it[Model3AuthorReceivedComments.authorId] }
                                                .toSet()
                                                .toList())
                            }).map { it[Posts.authorId]!! }
                            .toList()))
            posts.addAll(Posts.slice(Posts.authorId, Posts.id)
                    .selectAll()
                    .map { M3Post(it[Posts.id], it[Posts.authorId]!!) }
                    .toList())
            comments.addAll(Comments.slice(Comments.authorId, Comments.postId)
                    .selectAll()
                    .map { M3Comment(it[Comments.postId]!!, it[Comments.authorId]!!) }
                    .toList()
            )
        }

        authors.parallelStream()
                .forEach { id ->
                    val resultMap: MutableMap<Int, Int> = HashMap()
                    val authorPosts: Set<Int> = posts.filter { it.authorId == id }
                            .map { it.id }.toSet()
                    comments.filter { authorPosts.contains(it.postId) }
                            .forEach { comment ->
                                val commentId = comment.authorId
                                if (commentId != id) {
                                    if (!resultMap.containsKey(commentId)) {
                                        resultMap[commentId] = 0
                                    }
                                    resultMap[commentId] = resultMap[commentId]!!.plus(1)
                                }
                            }
                    if (resultMap.isNotEmpty()) {
                        transaction {
                            Utils.printBlueOutput("Insertting results for author.id = $id")
                            addLogger(StdOutSqlLogger)
                            resultMap.forEach { (commentator, value) ->
                                Model3AuthorReceivedComments.insert {
                                    it[Model3AuthorReceivedComments.authorId] = id
                                    it[Model3AuthorReceivedComments.commentatorId] = commentator
                                    it[Model3AuthorReceivedComments.commentsCount] = value
                                }
                            }
                        }
                    }
                }
    }

    fun countResults() {
        val results = HashSet<M3AuthorResult>()
        val authors = HashSet<M3Author>()
        val authorsNames = HashMap<Int, String>()
        val posts = HashSet<M3Post>()
        transaction {
            Utils.printBlueOutput("Counting results for model 3")
            addLogger(StdOutSqlLogger)
            SchemaUtils.createMissingTablesAndColumns(Model3Results)
            Authors.selectAll().forEach { authorsNames[it[Authors.id]] = it[Authors.name] ?: "" }
            posts.addAll(Posts.slice(Posts.authorId, Posts.id)
                    .selectAll()
                    .map { M3Post(it[Posts.id], it[Posts.authorId]!!) }
                    .toList())
            authors.addAll(
                    Model3AuthorReceivedComments
                            .slice(
                                    Model3AuthorReceivedComments.authorId,
                                    Model3AuthorReceivedComments.commentsCount.sum(),
                                    Model3AuthorReceivedComments.commentatorId.count()
                            )
                            .select(Op.build {
                                Model3AuthorReceivedComments.authorId notInList
                                        (Model3Results.slice(Model3Results.authorId)
                                                .selectAll()
                                                .map { it[Model3Results.authorId] })
                            })
                            .groupBy(Model3AuthorReceivedComments.authorId)
                            .map {
                                M3Author(
                                        it[Model3AuthorReceivedComments.authorId],
                                        authorsNames[it[Model3AuthorReceivedComments.authorId]] ?: "",
                                        it[Model3AuthorReceivedComments.commentsCount.sum()] ?: 0,
                                        it[Model3AuthorReceivedComments.commentatorId.count()]
                                )
                            }

            )
        }
        authors.forEach { it ->
            results.add(M3AuthorResult(it.id, it.name, it.receivedCommCount, it.diffUsers, posts.filter { post -> post.authorId == it.id }.count()))
        }
        val resultSortedByRank1: List<M3AuthorResult> = results.toList().sortedBy { it.diffUsers }.reversed()
        val resultSortedByRank2: List<M3AuthorResult> = results.toList().sortedBy { it.receivedCommCount / it.postCount }.reversed()
        results.forEach { author ->
            transaction {
                addLogger(StdOutSqlLogger)
                Model3Results.insert {
                    it[Model3Results.authorId] = author.id
                    it[Model3Results.name] = author.name
                    it[Model3Results.commentsCount] = author.receivedCommCount
                    it[Model3Results.postsCount] = author.postCount
                    it[Model3Results.differentCommentators] = author.diffUsers
                    it[Model3Results.rank1] = resultSortedByRank1.indexOfFirst { res -> res.id == author.id }
                    it[Model3Results.rank2] = resultSortedByRank2.indexOfFirst { res -> res.id == author.id }
                }
            }
        }
        Utils.printBlueOutput("End of counting results for model 3")
    }

    data class M3AuthorResult(val id: Int, val name: String, val receivedCommCount: Int, val diffUsers: Int, val postCount: Int, var rank1: Int = 0, var rank2: Int = 0)
    data class M3Author(val id: Int, val name: String, val receivedCommCount: Int, val diffUsers: Int)
    data class M3Post(val id: Int, val authorId: Int)
    data class M3Comment(val postId: Int, val authorId: Int)
}