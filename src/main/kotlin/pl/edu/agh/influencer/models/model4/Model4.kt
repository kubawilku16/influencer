package pl.edu.agh.influencer.models.model4

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import pl.edu.agh.influencer.entities.Authors
import pl.edu.agh.influencer.entities.Comments
import pl.edu.agh.influencer.entities.Posts
import pl.edu.agh.influencer.models.model3.Model3Results
import pl.edu.agh.influencer.utlis.Utils
import java.util.*
import kotlin.collections.HashSet

class Model4 {

    fun countModel4() {
        var authorsCount = 0
        val m3Results = LinkedList<M3Author>()
        val m3Ids = HashSet<Int>()

        transaction {
            Utils.printBlueOutput("Resolving model 4")
            addLogger(StdOutSqlLogger)
            SchemaUtils.createMissingTablesAndColumns(Model4Results)
            authorsCount = Authors.selectAll().count()
            Model3Results.slice(Model3Results.authorId, Model3Results.name, Model3Results.postsCount, Model3Results.rank1).selectAll()
                    .forEach {
                        m3Ids.add(it[Model3Results.authorId])
                        m3Results.add(M3Author(it[Model3Results.authorId], it[Model3Results.name], it[Model3Results.postsCount], it[Model3Results.rank1]))
                    }
        }
        m3Ids.addAll(m3Results.map { it.id })
        m3Results.forEach { m3 ->
            var result = 0.0
            transaction {
                addLogger(StdOutSqlLogger)
                (Comments innerJoin Posts).slice(Comments.authorId).select(Op.build { Posts.authorId eq m3.id }).forEach {
                    val id = it[Comments.authorId]
                    result += if (m3Ids.contains(id)) {
                        1 / (m3Results.first { res -> res.id == id }.rank1.toDouble() + 1.0)
                    } else {
                        1 / authorsCount.toDouble()
                    }
                }
                Model4Results.insert {
                    it[Model4Results.id] = m3.id
                    it[Model4Results.name] = m3.name
                    it[Model4Results.result] = result
                }
            }
        }

    }

    data class M3Author(val id: Int, val name: String, val postCount: Int, val rank1: Int)
    data class Post(val id: Int, val authorId: Int)
    data class Comment(val postId: Int, val authorId: Int)
}