package pl.edu.agh.influencer.entities

import org.jetbrains.exposed.sql.Table

object Tags : Table() {
    val id = integer("id").primaryKey()
    val name = varchar("name", length = 255).nullable()
}

object Authors : Table(){
    val id = integer("id").primaryKey()
    val blogLink = varchar("bloglink", length = 255).nullable()
    val name = varchar("name", length = 255).nullable()
}

object Comments : Table(){
    val id = integer("id").primaryKey()
    val content = text("content").nullable()
    val date = datetime("date")
    val title = varchar("title", length = 255).nullable()
    val authorId = (integer("author_id") references Authors.id).nullable()
    val parrentId = (integer("parentcomment_id") references Comments.id).nullable()
    val postId = (integer("post_id") references Posts.id).nullable()
    val sentiment = float("sentiment").nullable()
    val sentiment2 = float("sentiment2").nullable()
    val salonId = integer("salon_id").nullable()

}

object Posts : Table(){
    val id = integer("id").primaryKey()
    val content = text("content").nullable()
    val date = datetime("date")
    val link = varchar("link", length = 255).nullable()
    val title = varchar("title", length = 255).nullable()
    val authorId = (integer("author_id") references Authors.id).nullable()
    val sentiment = float("sentiment").nullable()
    val titleSentiment = float("titlesentiment").nullable()
    val sentiment2 = float("sentiment2").nullable()
    val titleSentiment2 = float("titlesentiment2").nullable()
}

object PostTags: Table(name = "posts_tags"){
    val postsId = integer("posts_id") references Posts.id
    val tagsId = integer("tags_id") references Tags.id
}