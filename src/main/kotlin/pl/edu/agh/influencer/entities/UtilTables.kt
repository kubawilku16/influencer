package pl.edu.agh.influencer.entities

import org.jetbrains.exposed.dao.*

object TimeStamps: IntIdTable(){
    val startDate = date("start_date")
    val endDate = date("end_date")
    val comments = integer("comments")
    val posts = integer("posts")
}

class TimeStamp(id: EntityID<Int>): IntEntity(id){
    companion object: IntEntityClass<TimeStamp>(TimeStamps)
    var start by TimeStamps.startDate
    var end by TimeStamps.endDate
    var comments by TimeStamps.comments
    var posts by TimeStamps.posts
}
