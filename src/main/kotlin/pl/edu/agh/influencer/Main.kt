package pl.edu.agh.influencer

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import pl.edu.agh.influencer.entities.*
import pl.edu.agh.influencer.models.model1.Model1
import pl.edu.agh.influencer.models.model2.Model2
import pl.edu.agh.influencer.models.model3.Model3
import pl.edu.agh.influencer.models.model4.Model4
import pl.edu.agh.influencer.models.model5.Model5
import pl.edu.agh.influencer.models.model5.small.Model5Small
import pl.edu.agh.influencer.models.model6.Model6
import pl.edu.agh.influencer.utlis.Utils.Companion.printBlueOutput
import java.io.File
import java.io.FileWriter

class Main {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Database.connect(
                    url = "jdbc:postgresql://localhost:5432/salon24db",
                    driver = "org.postgresql.Driver",
                    user = args[0],
                    password = args[1])
            printBlueOutput("Database connected")
//            resolveModel1()
//            resolveModel2()
//            resolveModel3()
//            resolveModel4()
//            resolveTimestamps()
//            resolveModel5()
            resolveModel6()
        }

        private fun resolveModel6() {
//            Model6().execute()
            Model6().extractResults()
        }

        private fun resolveModel5() {
//            Model5Small().execute()
            Model5Small().extractResults()
        }

        private fun resolveModel1() {
            val model1 = Model1()
            model1.countAndInsertTags()
            model1.countPostsImportanceDependsOnTagsValues()
//            model1.countAuthorsData()
//            model1.saveTopXResultsToFile(100)
        }

        private fun resolveModel2() {
            val model2 = Model2()
            model2.countAndInsertTags()
            model2.countPostsImportanceDependsOnTagsValues()
            model2.countSummaryForAuthors()
            model2.countLongPeriodForAuthors()
            model2.saveTopXResultsToFile(100)
        }

        private fun resolveModel3() {
            val model3 = Model3()
            model3.countModel3()
            model3.countModel3Parallel()
            model3.countResults()
        }

        private fun resolveModel4() {
            val model4 = Model4()
            model4.countModel4()
        }

        private fun printInitialDataCount() {
            transaction {
                println("Read all tables count:")
                addLogger(StdOutSqlLogger)
                val tagsCount = Tags.selectAll().count()
                val postsTagsCount = PostTags.selectAll().count()
                val postsCount = Posts.selectAll().count()
                val authorsCount = Authors.selectAll().count()
                val commentsCount = Comments.selectAll().count()
                printBlueOutput("\tPosts: $postsCount\n\tAuthors: $authorsCount\n\tComments: $commentsCount\n" +
                        "\n\tTags: $tagsCount\n\tNumber of tags usage: $postsTagsCount")

            }
        }

        private fun resolveTimestamps() {
            transaction {
                addLogger(StdOutSqlLogger)
                if (!TimeStamps.exists()) {
                    SchemaUtils.createMissingTablesAndColumns(TimeStamps)
                    val dates = Posts.slice(Posts.date).selectAll().orderBy(Posts.date)
                    val posts = Posts.selectAll()
                    val comments = Comments.selectAll()
                    var startPeriod = dates.first()[Posts.date]
                    var midPeriod = startPeriod.plusWeeks(1)
                    val endPeriod = dates.last()[Posts.date]
                    while (!startPeriod.isAfter(endPeriod)) {
                        TimeStamp.new {
                            start = startPeriod
                            end = midPeriod
                            this.posts = posts.adjustWhere { Op.build { Posts.date greaterEq startPeriod and Op.build { Posts.date less midPeriod } } }.count()
                            this.comments = comments.adjustWhere { Op.build { Comments.date greaterEq startPeriod and Op.build { Comments.date less midPeriod } } }.count()
                        }
                        startPeriod = midPeriod
                        midPeriod = midPeriod.plusWeeks(1)
                    }
                }
                val file = File("timestamps.csv")
                if (!file.exists()) {
                    val writer = FileWriter(file)
                    try {
                        file.createNewFile()
                        writer.append("Timestamp, comments, posts\n")
                        TimeStamps.selectAll()
                                .forEach { result ->
                                    writer.append("${result[TimeStamps.startDate].toString("dd-MM-YYYY")} - ${result[TimeStamps.endDate].toString("dd-MM-YYYY")}, ")
                                    writer.append("${result[TimeStamps.comments]}, ")
                                    writer.append("${result[TimeStamps.posts]}\n")
                                }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    } finally {
                        writer.flush()
                        writer.close()
                    }
                }
            }
        }
    }
}